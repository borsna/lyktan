<?php
/**
 * @file
 *  Get the lunch menu from Lyktan at university of Gothenburg
 *
 * @author  Olof Olsson <borsna@gmail.com>
 * @date    2018-10-25
 * @license MIT
 */

namespace Lyktan;

use Smalot\PdfParser\Parser;

class Lunch {
  private static $host = 'https://www.gu.se';
  private static $page = '/motesservice/matochdryck/dagenslunch-pa-lyktan';
  private static $days = ['Måndag', 'Tisdag', 'Onsdag', 'Torsdag', 'Fredag', 'Lördag', 'Söndag'];

  /**
   * Get current name of the day
   *
   * @return string
   */
  public function currentDay():string{
    return self::$days[(int)date('N')-1];
  }

  /**
   * Get todays lunch options
   *
   * @return string
   */
  public function today():string{
    $menu = self::menu();
    $day = self::currentDay();

    if(array_key_exists($day, $menu)){
      return $menu[$day];
    }

    return 'No information';
  }

  /**
   * Get array of all menu items
   *
   * @return array
   */
  public function menu():array{
    $result = [];

    $parser = new \Smalot\PdfParser\Parser();
    $pdf = $parser->parseFile(self::pdfLink());
    
    $pages = $pdf->getPages();

    $day = '';
    foreach ($pages as $page) {
      $page_text = trim($page->getText());
        
      foreach(explode("\n", $page_text) as $row){
        $row = trim($row);
        if(in_array($row, self::$days)){
          $day = $row;
          $result[$day] = '';
        }elseif(!empty($day)){
          if(self::startsWithUpper($row)){
            $result[$day] .= "\n";
          }
          $result[$day] .= self::fixRow($row);
        }
        
        if(empty($row)){
          $day = '';
        }
      }
    }

    return $result;
  }

  /**
   * Find the pdf link for the menu
   *
   * @return string
   */
  public function pdfLink():string {
    libxml_use_internal_errors(true);
    $document = new \DOMDocument();
    $document->loadHTMLFile(self::$host.self::$page);
    
    $xpath = new \DOMXpath($document);
    
    $links = $xpath->query("//a");
    
    $pdf_links = [];
    foreach($links as $link) {
      if($link->getAttribute('class') === 'pdficon'){
        $pdf_links[] = $link->getAttribute('href');
      }
    }
    
    return self::$host.$pdf_links[0];
  }

  /**
   * Checks if first character is upper case
   *
   * @param string $string
   * @return boolean
   */
  private static function startsWithUpper(string $string):bool {
    $char = mb_substr($string, 0, 1, "UTF-8");
    
    return mb_strtolower($char, "UTF-8") != $char;
  }
  
  /**
   * Fix formating for string from the pdf menu
   *
   * @param string $row
   *   Text row to fix formating in
   * @return string 
   *   Fixed string
   */
  private static function fixRow(string $row):string{
    $row = preg_replace('/\s+/', ' ', $row);
    $row = str_replace(" | ","|", $row);
    $row = str_replace(" |","|", $row);
    $row = str_replace("|",", ", $row);

    return $row;
  }
}