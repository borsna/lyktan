# Todays lunch at lyktan

This script finds the pdf link from: https://www.gu.se/motesservice/matochdryck/dagenslunch-pa-lyktan and parses the text to find todays lunch.

Can be called via CLI:

`php lyktan/index.php`

or just by calling it via the web.