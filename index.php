<?php
include 'vendor/autoload.php';
include 'src/Lyktan.php';

use Lyktan\Lunch;

//Print todays menu if called via cli
if(php_sapi_name() == "cli") {
  print Lunch::currentDay().PHP_EOL;
  print Lunch::today();
  exit(0);
}
?>

<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Dagens lunch på lyktan</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<style>
			body{
				font-family: "Open Sans",sans-serif;
				color: #484848;
			}
		</style>
	</head>

	<body>
		<!-- html representation of the lunch menu at lyktan -->
		<strong class="day"><?php print Lunch::currentDay();?></strong>
		<span class="menu">
		  <?php print nl2br(Lunch::today());?>
		</span>
		<br />
		<a class="pdflink" href="<?php print Lunch::pdfLink();?>">pdf</a>
	</body>
</html>
